from airflow import DAG
from airflow.providers.mysql.hooks.mysql import MySqlHook
from airflow_clickhouse_plugin.hooks.clickhouse_hook import ClickHouseHook
from airflow.operators.python_operator import PythonOperator
import pandas as pd
from datetime import datetime

dict_extract_params = {'query_extract_1': """SELECT t.*, g.*
                                FROM title AS t 
                                INNER JOIN title_group AS tg 
                                ON t.ID = tg.title_ID 
                                INNER JOIN `group` AS g
                                ON g.ID = tg.group_ID;""",
                'query_extract_2': """SELECT t.*, tar.*
                                FROM title AS t
                                INNER JOIN title_target AS tt
                                ON tt.title_ID = t.ID
                                INNER JOIN target AS tar
                                ON tar.ID = tt.target_ID""",
                                }

PATH_TO_FILE = 'home/trecer/airflow/temp_data/temp.csv'


# def define_type(row):
#     row = str(row)
#     if row == 'object':
#         return 'text'
#     elif row == 'int64':
#         return 'Int'
#     elif row == 'float64':
#         return 'real'
#     elif row == 'datetime64[ns]':
#         return 'date'


# def create_query(df, name):
#     query_create = f'CREATE TABLE IF NOT EXISTS {name} (`index` SERIAL PRIMARY KEY, '  
#     names_col_and_types = list(zip(df.columns, list(map(define_type, df.dtypes))))
#     for name_col, name_type in names_col_and_types:
#         if (name_col, name_type) != names_col_and_types[-1]:
#             query_create +='`' + name_col + '`' + ' ' + name_type + ', '
#         else:
#             return query_create + '`' + name_col + '`' + ' ' + name_type + ');'


def define_type(row):
    """Функция define_type 
        изменяет типы данных pandas в тип данных clickhouse
        На вход принимает имя типа данных как строку row
        Возвращает тип данных clickhouse"""
    row = str(row)
    if row == 'object':
        return 'String'
    elif row == 'int64':
        return 'Int64'
    elif row == 'float64':
        return 'Float64'
    elif row == 'datetime64[ns]':
        return 'DateTime'


def create_query(df, name):
    """Функция create_query 
    создает запрос на создание таблицы в clickhouse
    На вход принимает датафрейм df и имя будущей таблицы name
    Возвращает строку-запрос"""
    query_create = f'CREATE TABLE IF NOT EXISTS {name} ('
    names_col_and_types = list(zip(df.columns, list(map(define_type, df.dtypes))))
    for name_col, name_type in names_col_and_types:
        if (name_col, name_type) != names_col_and_types[-1]:
            query_create +='`' + name_col + '`' + ' ' + name_type + ', '
        else:
            return query_create + '`' + name_col + '`' + ' ' + name_type + f') ENGINE = MergeTree() ORDER BY `{df.columns[0]}`;'


def change_table(df, hook, name):
    hook.run(create_query(df, name))
    is_empty = int(hook.run(f'SELECT COUNT(*) FROM {name}')[0][0])
    if is_empty != 0:
        hook.run(create_query(df, f'new_{name}'))
        hook.run(f'DROP TABLE {name}')
        hook.run(f"""RENAME TABLE new_{name} TO {name}""")

    hook.run(f"INSERT INTO {name} VALUES;", df.to_dict('records'))


def extract(query_extract_1, query_extract_2):
    extract_hook = MySqlHook('ab_testing')
    df_extract_1 = extract_hook.get_pandas_df(query_extract_1)
    df_extract_2 = extract_hook.get_pandas_df(query_extract_2)
    df_extract = pd.merge(left=df_extract_1, 
                            right=df_extract_2, 
                            how='inner', 
                            left_index=True,
                            right_index=True)
    # Сохранение данных на сервере
    df_extract.to_csv(PATH_TO_FILE)


def preprocess(df_transform):
    df_transform = df_transform[['type_group', 'title_y',	'target']]
    df_transform.loc[df_transform['type_group'] == 'mobile', 'type_group'] = 'test'
    df_transform.loc[df_transform['type_group'] == 'desktop', 'type_group'] = 'control'
    df_transform.rename(columns={'title_y': 'title'}, inplace=True)
    return df_transform


def transform_and_load(path_to_file, name_of_created_table):
    df_transform = pd.read_csv(path_to_file)
    # preprocessing
    df_load = preprocess(df_transform)
    # Создаем таблицу в базе данных clickhouse и заполняем таблицу
    clickhouse_hook = ClickHouseHook('default')
    change_table(df_load, clickhouse_hook, name_of_created_table)


with DAG(dag_id='dag_ab_testing',
         schedule_interval='0 0 1 * *', # Интервал запусков
         start_date=datetime(2022, 4, 30) # Начальная точка запуска
    ) as dag:


    t1 = PythonOperator(
        task_id = 'task_extract',
        python_callable = extract,
        op_kwargs = dict_extract_params
    )

    t2 = PythonOperator(
        task_id = 'task_transform',
        python_callable = transform_and_load,
        op_kwargs = {'path_to_file': PATH_TO_FILE,
                     'name_of_created_table': 'ab_result_table'},
        
    )

t1 >> t2